package com.voxelbuster.ftpclient.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.voxelbuster.ftpclient.R;
import com.voxelbuster.ftpclient.data.ServerEntriesList;
import com.voxelbuster.ftpclient.data.ServerEntryManager;
import com.voxelbuster.ftpclient.data.ServerEntryModel;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

public class EditServerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_server);
        this.getSupportActionBar().setTitle(R.string.add_server);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        this.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close_white_32dp);

        Intent parentIntent = getIntent();
        if (parentIntent.hasExtra("target")) {
            prefillForm(parentIntent.getStringExtra("target"));
        }

        EditText passEdit = findViewById(R.id.passEdit);
        passEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                CheckBox askPassCb = findViewById(R.id.askPasswordCheck);
                if (charSequence.length() == 0) {
                    askPassCb.setEnabled(true);
                } else if (charSequence.length() > 0) {
                    askPassCb.setEnabled(false);
                    askPassCb.setSelected(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private void prefillForm(String uuid) {
        ServerEntriesList list = ServerEntryManager.getServerEntriesList();
        ServerEntryModel entry = list.get(uuid);
        EditText name = findViewById(R.id.serverNameEdit);
        name.setText(entry.getName());
        EditText host = findViewById(R.id.hostEdit);
        host.setText(entry.getHost());
        EditText port = findViewById(R.id.portEdit);
        port.setText("" + entry.getPort());
        EditText user = findViewById(R.id.userEdit);
        user.setText(entry.getUser());
        EditText pass = findViewById(R.id.passEdit);
        pass.setText(entry.getPass());

        Spinner protocol = findViewById(R.id.protocolSpinner);
        protocol.setSelection(entry.getProtocol());

        CheckBox askPassword = findViewById(R.id.askPasswordCheck);
        askPassword.setSelected(entry.doesAskForPassword());

        CheckBox isPasv = findViewById(R.id.passiveModeCheck);
        isPasv.setSelected(entry.isPasv());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_server, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;

            case R.id.action_save:
                EditText name = findViewById(R.id.serverNameEdit);
                EditText host = findViewById(R.id.hostEdit);
                EditText port = findViewById(R.id.portEdit);
                EditText user = findViewById(R.id.userEdit);
                EditText pass = findViewById(R.id.passEdit);

                Spinner protocol = findViewById(R.id.protocolSpinner);

                CheckBox askPassword = findViewById(R.id.askPasswordCheck);
                CheckBox pasv = findViewById(R.id.passiveModeCheck);

                String portString;
                if (port.getText().length() == 0) {
                    portString = "21";
                } else {
                    portString = port.getText().toString();
                }

                ServerEntryModel entry = new ServerEntryModel(name.getText().toString(), host.getText().toString(),
                        Integer.parseInt(portString), pasv.isSelected(), askPassword.isSelected(),
                        protocol.getSelectedItemPosition(), user.getText().toString(), pass.getText().toString());

                ServerEntriesList list = ServerEntryManager.getServerEntriesList();
                list.add(entry);
                ServerEntryManager.setServerEntriesList(list);

                Toast.makeText(this, "" + ServerEntryManager.getServerEntriesList().size(), Toast.LENGTH_SHORT).show();

                try {
                    ServerEntryManager.serialize();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
