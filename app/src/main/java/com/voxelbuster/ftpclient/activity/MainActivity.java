package com.voxelbuster.ftpclient.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.voxelbuster.ftpclient.R;
import com.voxelbuster.ftpclient.adapter.ServerArrayAdapter;
import com.voxelbuster.ftpclient.data.ServerEntriesList;
import com.voxelbuster.ftpclient.data.ServerEntryManager;
import com.voxelbuster.ftpclient.data.ServerEntryModel;

import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private ServerArrayAdapter serverArrayAdapter;
    private LinearLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            ServerEntryManager.init(this, "server_list.json");
            ServerEntryManager.deserialize();
        } catch (IOException e) {
            e.printStackTrace();
        }

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, EditServerActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        getSupportActionBar().setTitle(R.string.app_name_full);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        notifyListChanged();
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            ServerEntryManager.serialize();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void notifyListChanged() {
        RecyclerView serverLV = findViewById(R.id.serversList);
        inflateServerList(serverLV);
        serverArrayAdapter.notifyDataSetChanged();
    }

    private void inflateServerList(RecyclerView lv) {
        ServerEntriesList list = ServerEntryManager.getServerEntriesList();

        this.serverArrayAdapter = new ServerArrayAdapter(this);

        if (list.size() > 0) {
            for (ServerEntryModel model : list.getAll()) {
                serverArrayAdapter.addServer(model);
            }
        }

        lv.setAdapter(serverArrayAdapter);
        layoutManager = new LinearLayoutManager(this);
        lv.setLayoutManager(layoutManager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_exit) {
            finishAndRemoveTask();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
