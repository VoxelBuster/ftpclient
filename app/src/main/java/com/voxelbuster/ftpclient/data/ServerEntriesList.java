package com.voxelbuster.ftpclient.data;

import java.util.ArrayList;

public final class ServerEntriesList {
    private final String dummy = "_";
    private final ArrayList<ServerEntryModel> entries = new ArrayList<>();
    public ServerEntriesList() {
    }

    public void add(ServerEntryModel entry) {
        entries.add(entry);
    }

    public ServerEntryModel add(String name, String host, int port, boolean pasv, boolean askPass, int protocol, String user, String pass) {
        ServerEntryModel entry = new ServerEntryModel(name, host, port, pasv, askPass, protocol, user, pass);
        entries.add(entry);
        return entry;
    }

    public boolean remove(int index) {
        return entries.remove(index) != null;
    }

    public boolean remove(ServerEntryModel model) {
        return entries.remove(model);
    }

    public boolean remove(String uuid) {
        ServerEntryModel toRemove = null;
        for (ServerEntryModel e : entries) {
            if (e.getUuid().toString().equals(uuid)) {
                toRemove = e;
                break;
            }
        }
        if (toRemove != null) {
            return remove(toRemove);
        } else {
            return false;
        }
    }

    public ServerEntryModel get(int index) {
        return entries.get(index);
    }

    public ServerEntryModel get(String uuid) {
        for (ServerEntryModel e : entries) {
            if (e.getUuid().toString().equals(uuid)) {
                return e;
            }
        }
        return null;
    }

    public int size() {
        return entries.size();
    }

    public ServerEntryModel[] getAll() {
        return entries.toArray(new ServerEntryModel[0]);
    }
}
