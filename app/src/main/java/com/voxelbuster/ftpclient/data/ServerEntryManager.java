package com.voxelbuster.ftpclient.data;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class ServerEntryManager {
    private static ServerEntriesList serverEntriesList;
    private static String targetFilePath;
    private static Context context;

    public static void init(Context c, String targetFilePath) throws IOException {
        context = c;
        ServerEntryManager.targetFilePath = targetFilePath;
        deserialize();
    }

    public static void serialize() throws IOException {
        if (targetFilePath == null) {
            throw new IllegalStateException("init(String) must be called first!");
        }
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        FileOutputStream targetFileWrite = context.openFileOutput(targetFilePath,
                context.MODE_PRIVATE);
        OutputStreamWriter osw = new OutputStreamWriter(targetFileWrite);
        gson.toJson(serverEntriesList, osw);
        if (gson.fromJson(gson.toJson(serverEntriesList), ServerEntriesList.class) == null) {
            Toast.makeText(context, "Gson can\'t read its own code", Toast.LENGTH_SHORT);
        }
        osw.close();
        targetFileWrite.close();
    }

    public static void deserialize() throws IOException {
        if (targetFilePath == null) {
            throw new IllegalStateException("init(String) must be called first!");
        }
        Gson gson = new GsonBuilder().setLenient().create();
        File file = new File(context.getFilesDir(), targetFilePath);
        FileReader fr = new FileReader(file);
        if (!file.exists()) {
            serverEntriesList = new ServerEntriesList();
        } else {
            serverEntriesList = gson.fromJson(fr, ServerEntriesList.class);
        }

        if (serverEntriesList == null) {
            serverEntriesList = new ServerEntriesList();
        }
    }

    public static ServerEntriesList getServerEntriesList() {
        return serverEntriesList;
    }

    public static void setServerEntriesList(ServerEntriesList serverEntriesList) {
        ServerEntryManager.serverEntriesList = serverEntriesList;
    }
}
