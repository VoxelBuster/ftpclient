package com.voxelbuster.ftpclient.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.voxelbuster.ftpclient.R;
import com.voxelbuster.ftpclient.activity.EditServerActivity;
import com.voxelbuster.ftpclient.activity.MainActivity;
import com.voxelbuster.ftpclient.data.ServerEntryManager;
import com.voxelbuster.ftpclient.data.ServerEntryModel;
import com.voxelbuster.ftpclient.ui.CommonDialog;

import java.util.ArrayList;
import java.util.UUID;

public class ServerArrayAdapter extends RecyclerView.Adapter<ServerArrayAdapter.ServerViewHolder> {
    private final ArrayList<ServerEntryModel> list = new ArrayList<>();

    private final Context context;

    public ServerArrayAdapter(Context context) {
        super();
        this.context = context;
    }

    public void addServer(ServerEntryModel serverEntryModel) {
        list.add(serverEntryModel);
    }

    @NonNull
    @Override
    public ServerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ServerViewHolder(inflater.inflate(R.layout.server_listitem_layout, parent, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull ServerViewHolder holder, int position) {
        ServerEntryModel entry = list.get(position);
        holder.title.setText(entry.getName());
        holder.subtext.setText(entry.getHost() + ":" + entry.getPort());
        holder.serverId.setText(entry.getUuid().toString());
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public UUID getUuid(int i) {
        return list.get(i).getUuid();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void remove(int i) {
        list.remove(i);
    }

    public class ServerViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView title;
        TextView subtext;
        TextView serverId;

        public ServerViewHolder(@NonNull View v) {
            super(v);
            title = v.findViewById(R.id.server_item_title);
            subtext = v.findViewById(R.id.server_item_subtext);
            serverId = v.findViewById(R.id.server_id);
            v.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            PopupMenu popup = new PopupMenu(view.getContext(), view);
            new MenuInflater(view.getContext()).inflate(R.menu.menu_server_click, popup.getMenu());
            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    int id = item.getItemId();
                    if (id == R.id.action_connect) {
                        return true;
                    } else if (id == R.id.action_edit) {
                        Intent intent = new Intent(context, EditServerActivity.class);
                        intent.putExtra("target", serverId.getText().toString());
                        context.startActivity(intent);
                        return true;
                    } else if (id == R.id.action_delete) {
                        CommonDialog.createYesNo(context, "Delete?", "Are you sure you " +
                                "want to delete entry " + title.getText().toString() + "?", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ServerEntryManager.getServerEntriesList().remove(serverId.getText().toString());
                                if (context instanceof MainActivity) {
                                    ((MainActivity)context).notifyListChanged();
                                }
                            }
                        }, null).show();
                        return true;
                    } else {
                        return false;
                    }
                }
            });
            popup.show();
        }
    }
}
